package com.cgi.model;

public class Node {

	private int id;
	private String name;
	private int alcoholLevel;
	private int alcoholTolerance;
	private boolean toleranceUsed;
	private String description;
	private String goods;
	private int[] connections;

	/**
	 * Constructor for building new node
	 * @param id
	 * @param name
	 * @param alcoholLevel - how much alcohol will player accumulate at this node
	 * @param alcoholTolerance - how much alcohol tolerance will acumulate player at this node
	 * @param toleranceUsed checker for preventing multiple alcohol tolerances gain
	 * @param description
	 * @param goods - what player will buy here
	 * @param connections - all posible nodes that player can access
	 */
	public Node(int id, String name, int alcoholLevel, int alcoholTolerance,boolean toleranceUsed, String description, String goods,
			int[] connections) {
		super();
		this.id = id;
		this.name = name;
		this.alcoholLevel = alcoholLevel;
		this.alcoholTolerance = alcoholTolerance;
		this.toleranceUsed = false;
		this.description = description;
		this.goods = goods;
		this.connections = connections;
	}
	/**
	 * Empty constructor needed for loading nodes from file.
	 */
	public Node()
	{
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAlcoholLevel() {
		return alcoholLevel;
	}

	public void setAlcoholLevel(int alcoholLevel) {
		this.alcoholLevel = alcoholLevel;
	}

	public int getAlcoholTolerance() {
		return alcoholTolerance;
	}

	public void setAlcoholTolerance(int alcoholTolerance) {
		this.alcoholTolerance = alcoholTolerance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGoods() {
		return goods;
	}

	public void setGoods(String goods) {
		this.goods = goods;
	}

	public int[] getConnections() {
		return connections;
	}

	public void setConnections(int[] connections) {
		this.connections = connections;
	}

	public boolean isToleranceUsed() {
		return toleranceUsed;
	}

	public void setToleranceUsed() {
		this.toleranceUsed = true;
	}

}
