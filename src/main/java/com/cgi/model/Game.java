package com.cgi.model;

import java.util.*;

/**
 * @author CGI
 *
 */
public class Game {

	private Player player;
	private List<Node> nodes;

	/**
	 * Constructor for creating new game, takes care of creating new player,loading nodes data and executing game
	 */
	public Game() {
		Loader loader = new Loader();
		this.nodes = loader.readFromFile();
		if (nodes.size() > 0) {
			System.out.println("Nodes loaded.");
		} else {
			System.out.println("Problem with loading nodes.");
		}
		System.out.println("========================================");
		System.out.println("Please write your name:");
		Scanner scanner = new Scanner(System.in);
		this.player = new Player(scanner.nextLine());

		System.out.println("========================================");
		player.setPosition(nodes.get(0));
		System.out.println(player.toString());
		System.out.println("========================================");

		play(scanner);

	}

	/**
	 * Method for checking winning/losing conditions, requires scanner from Game() constructor
	 * @param scanner
	 */
	public void play(Scanner scanner) {
		while (true) {

			if (player.getPosition().getId() == 20) {
				System.out.println("You seems to be at " + player.getPosition().getName()
						+ ". Congratulations, You arrived at your destination.");
				break;
			} else if (player.getAlcohol() > player.getTolerance()) {
				System.out.println("You seems to be at Hospital. This was not the destination /n you wanted to arrive."
						+ "Doctors told you you had a bad case of alcohol poisoning. ");
				System.out.println(" Y O U   L O S E");
				break;

			} else {
				travel(scanner);
			}

		}

	}

	/**
	 * Method for traveling between nodes, requires scanner from play() method
	 * @param scanner
	 */
	public void travel(Scanner scanner) {
		System.out.println("You seems to be at " + player.getPosition().getName());
		System.out.println(player.getPosition().getDescription());
		player.addAlcohol(player.getPosition().getAlcoholLevel());
		player.addTolerance(player.getPosition().getAlcoholTolerance());
		System.out.println("Where do you want to go?");
		for (int i = 0; i < player.getPosition().getConnections().length; i++) {
			System.out.println((i + 1) + ". " + nodes.get(player.getPosition().getConnections()[i]).getName());
		}
		System.out.println("Write number of location where you want to go.");
		String scan = scanner.nextLine();
		int convert = stringToInt(scan);
		int location = convert - 1;
		while (true) {
			if ((location < 0) || (location > player.getPosition().getConnections().length)) {
				System.out.println();
				System.out.println("Location does not exist. Write valid number of location where you want to go.");
				location = stringToInt(scanner.nextLine()) - 1;
			} else {
				player.setPosition(nodes.get(player.getPosition().getConnections()[location]));

				break;
			}

		}

	}

	public int stringToInt(String string) {
		try {
			return Integer.valueOf(string);
		} catch (NumberFormatException e) {
			return 0;
		}

	}

}
