package com.cgi.model;

public class Player {

	private String name;
	private Node position;
	private int alcohol;
	private int tolerance;

	/**
	 * Constructor for creating new player requires name from scanner
	 * @param name
	 */
	public Player(String name) {
		this.name = name;
		this.position = null;
		this.alcohol = 0;
		this.tolerance = 100;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Node getPosition() {
		return position;
	}

	public void setPosition(Node position) {
		this.position = position;
	}

	public int getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(int alcohol) {
		this.alcohol = alcohol;
	}

	public int getTolerance() {
		return tolerance;
	}

	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}

	/**
	 * method for increasing/decreasing alcohol levels
	 * @param alcoholLvl
	 */
	public void addAlcohol(int alcoholLvl) {
		alcohol = alcohol+alcoholLvl;
		if(this.alcohol<0)
		{
			alcohol = 0;
		}

	}

	/**
	 * method for increasing alcohol tolerance
	 * @param alcoholLimit
	 */
	public void addTolerance(int alcoholLimit) {
		tolerance = tolerance + alcoholLimit;
	}

	@Override
	public String toString() {
		return "You are called " + name
				+ "\n . You won the lotery and decided to go celebrate it in the city. \n Your current location seems to be "
				+ position.getName();
	}

}
