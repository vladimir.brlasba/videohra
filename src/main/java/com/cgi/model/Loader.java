package com.cgi.model;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * @author CGI
 *
 */
public class Loader {

	

	public Loader() {
		
	}
	

	/*public void writeToFile(List<Node> nodes) {

		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(Paths.get("C:\\Users\\CGI\\eclipse-workspace\\videogame\\src\\main\\resources\\nodes.txt").toFile(), nodes);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

	/**
	 * Method for loading nodes from file.
	 * @List of nodes loaded from nodes.txt file.
	 */
	public List<Node> readFromFile() {
		ObjectMapper mapper = new ObjectMapper();
		List<Node> nodes = new ArrayList<>();
		InputStream in = getClass().getClassLoader().getResourceAsStream("nodes.txt");
		try {
			TypeFactory typeFactory = mapper.getTypeFactory();
			nodes = mapper.readValue(in,
					typeFactory.constructCollectionType(List.class, Node.class));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return nodes;
	}

}
